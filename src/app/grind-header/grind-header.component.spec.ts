import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrindHeaderComponent } from './grind-header.component';

describe('GrindHeaderComponent', () => {
  let component: GrindHeaderComponent;
  let fixture: ComponentFixture<GrindHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrindHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrindHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
