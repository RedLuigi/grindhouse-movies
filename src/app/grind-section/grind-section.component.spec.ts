import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrindSectionComponent } from './grind-section.component';

describe('GrindSectionComponent', () => {
  let component: GrindSectionComponent;
  let fixture: ComponentFixture<GrindSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrindSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrindSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
