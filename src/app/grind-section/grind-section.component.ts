import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'grind-section',
  templateUrl: './grind-section.component.html',
  styleUrls: ['./grind-section.component.scss']
})
export class GrindSectionComponent implements OnInit {

  @Input() public title: string

  constructor() { }

  ngOnInit() {
  }

}
