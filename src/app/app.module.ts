import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GrindHeaderComponent } from './grind-header/grind-header.component';
import { GrindSectionComponent } from './grind-section/grind-section.component';

@NgModule({
  declarations: [
    AppComponent,
    GrindHeaderComponent,
    GrindSectionComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
